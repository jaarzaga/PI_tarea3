#Import libraries to use
import cv2
import numpy as np
import os

#Declares main function
def main():
    # Declare global variables
    global thrs1, change, count

    #Initialize global and local variables
    thrs1 = 100
    count = 100
    change = 1
    opt="0"

    #Print out menu for user
    print("*******************************************************")
    print("*******************************************************")
    print("**  1) Edge detection (Canny)                        **")
    print("**  2) Lines detection (Hough)                       **")
    print("**  3) Circles detection (Hough)                     **")
    print("**  4) Corners detection (Harris)                    **")
    print("*******************************************************")
    print("*******************************************************")

    #Request user to input file name
    filename = input("Name of file to open (including extension): ")

    #Request user to select option from menu, validate selection
    while (opt<"1" or opt>"4"):
        opt = input("Select a Feature detection form menu: ")

    print("*******************************************************")
    print("**  Press 0 when finished to exit                    **")
    print("*******************************************************")

    #Open image file, displays it on new window
    dirname = os.path.dirname(__file__)
    filedir = os.path.join(dirname, filename)
    img = cv2.imread(filedir)
    cv2.namedWindow('Input Image')
    cv2.imshow('Input Image', img)

    #Create new window for procesed image
    cv2.namedWindow('Image Out')
    #Add trackbars to window in order to modify parameters for detection
    #Creates 2 or 1 according to feature selected
    if(opt!="4"):
        cv2.createTrackbar('thrs1', 'Image Out', 100, 2000, threshold)
    if(opt!="1"):
        cv2.createTrackbar('count', 'Image Out', 100, 400, counter)

    #Loop program until 0 is pressed,  to visualize and modify procesed image
    while( cv2.waitKey(200) != 0x30 ):

        #If any paramater is modified, re process image and clear change flag
        if(change):
            if(opt=="1"): img_out = edge_detector(img, thrs1)
            elif(opt=="2"): img_out = line_detector(img, thrs1+1, count+1)
            elif(opt=="3"): img_out = circle_detector(img, thrs1+1, count+1)
            elif(opt=="4"): img_out = corner_detector(img, count+1)
            change = 0

        #Display processed image
        cv2.imshow('Image Out', img_out)

    #Write processed image as jpg
    filename = filename[:-4]+"_"+str(opt)+"_"+str(thrs1)+"_"+str(count)+".jpg"
    cv2.imwrite(filename,img_out)

#Function to detect edges, receive image and threshold
def edge_detector(img,thrs1):
    #Applies canny algorithm for edge detection
    edges = cv2.Canny(img,thrs1/2,thrs1,apertureSize = 3)
    return edges

#Function to detect lines, receive image, threshold for canny algorithm
# and lenght of lines to be detected
def line_detector(img2, thrs1, count):
    img = img2.copy()
    # Applies canny algorithm for edge detection
    edges = cv2.Canny(img,thrs1/2,thrs1,apertureSize = 3)
    # Detect lines of given lenght on the edges
    lines = cv2.HoughLines(edges,1,np.pi/180,count)
    #Any line found, is drawn on the image
    if(np.any(lines)):
        for line in lines:
            rho,theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))
            cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
    return img

#Function to detect circles, receive image, threshold for canny algorithm
#and threshold for accumulator for circle centers
def circle_detector(img2, thrs1, count):
    #creates a grayscale and a color copy of image
    img = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    #Detect circles with given paramters on the image
    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, minDist=20, param1=thrs1, param2=count,
                               minRadius=0, maxRadius=0)
    #Any circle found, is drawn on the image, center and edge
    if(np.any(circles)):
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
    return cimg

#Function to detect corners, receives image and harris detector free
#free paramter (need to be divided by 200)
def corner_detector(img2, count):
    #creates a grayscale and a color copy of image
    gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    img = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

    # find Harris corners
    gray = np.float32(gray)
    dst = cv2.cornerHarris(gray, 2, 3, count/200)
    dst = cv2.dilate(dst, None)

    # Threshold for an optimal value, it may vary depending on the image.
    img[dst > 0.01 * dst.max()] = [0, 0, 255]
    return img

#Callback for trackbar of first parameter, threshold
def threshold(position, *argv):
    #When changed set change flag to re process image
    global thrs1, change
    thrs1 = position
    change = 1

#Callback for trackbar of second parameter, counter
def counter(position, *argv):
    #When changed set change flag to re process image
    global count, change
    count = position
    change = 1

#Call main function to initalize program
main()